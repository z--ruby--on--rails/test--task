# README

## Setup process
```
cp .env.sample .env
<edit .env file if necessary>
docker-compose build
docker-compose run --rm web bundle exec rails db:setup
docker-compose up
```

## Changing DB password on-the-go
```
docker-compose up --remove-orphans --force-recreate --build db
psql -h localhost -U postgres
ALTER ROLE postgres WITH PASSWORD 'your_password';
```

## Tests and linters
```
docker-compose run --rm web bundle exec rspec
docker-compose run --rm web bundle exec rubocop
docker-compose run --rm web bundle exec reek
docker-compose run --rm web bundle exec brakeman -z -q
docker-compose run --rm web bundle exec haml-lint app/views/
```
