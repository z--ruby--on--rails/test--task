# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'API V1 servers' do
  describe 'GET /api/v1/servers/' do
    subject :get_interfaces do
      get '/api/v1/servers/', headers: { 'Content-Type' => 'application/json' }
    end

    let(:servers) { create_list(:server, 3) }

    before do
      get_interfaces
    end

    it 'returns list of interfaces at this server' do
      expect(response.status).to eq(200)

      # TODO
    end
  end

  describe 'DELETE to /api/v1/servers/:server_id' do
    subject :delete_interface do
      delete "/api/v1/servers/#{server.id}", headers: { 'Content-Type' => 'application/json' }
    end

    let(:server) { create(:server) }

    before do
      delete_interface
    end

    it 'removes this interface from this server' do
      expect(response.status).to eq(200)

      # TODO
    end
  end
end
