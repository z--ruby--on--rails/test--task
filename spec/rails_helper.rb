# frozen_string_literal: true

require 'simplecov'
SimpleCov.start 'rails' do
  add_filter '/vendor/'
end

require 'spec_helper'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../config/environment', __dir__)
abort('The Rails environment is running in production mode!') if Rails.env.production?
require 'webmock/rspec'
require 'rspec/rails'

require 'capybara/rails'
require 'capybara/rspec'

Dir['./spec/support/**/*.rb'].sort.each { |file| require file }

begin
  ActiveRecord::Migration.maintain_test_schema!
rescue ActiveRecord::PendingMigrationError => exception
  puts exception.to_s.strip
  exit 1
end

RSpec.configure do |config|
  config.fixture_path = Rails.root.join('spec/fixtures')
  config.use_transactional_fixtures = true
  config.infer_spec_type_from_file_location!
  config.filter_rails_from_backtrace!

  config.include FactoryBot::Syntax::Methods

  Capybara.register_driver :chrome_headless do |app|
    options = Selenium::WebDriver::Chrome::Options.new(
      args: %w[
        headless
        no-sandbox
        disable-dev-shm-usage
        window-size=1920,1080
      ]
    )
    Capybara::Selenium::Driver.new(app, browser: :chrome, options: options)
  end
  Capybara.javascript_driver = :chrome_headless
  Capybara.default_max_wait_time = 10

  WebMock.disable_net_connect!(allow_localhost: true)

  config.before(:each, type: :system) do
    driven_by :rack_test
  end
  config.before(:each, type: :system, js: true) do
    driven_by :chrome_headless
  end
end
