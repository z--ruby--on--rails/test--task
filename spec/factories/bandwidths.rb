# frozen_string_literal: true

# == Schema Information
#
# Table name: bandwidths
#
#  id             :bigint           not null, primary key
#  interface_name :text             not null
#  value          :float            not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  server_id      :bigint
#  value_id       :integer          not null
#
# Indexes
#
#  index_bandwidths_on_interface_name  (interface_name)
#  index_bandwidths_on_server_id       (server_id)
#
FactoryBot.define do
  factory :bandwidth do
    sequence(:interface_name) { |n| "eth#{n}" }
    value { rand(100.0...1500.0) }
  end
end
