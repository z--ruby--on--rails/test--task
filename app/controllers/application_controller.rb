# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Pagy::Backend

  rescue_from ActiveRecord::RecordNotFound, with: :not_found_error
  rescue_from Pagy::OverflowError, with: :not_found_error

  def destroy
    # TODO

    head :ok
  end

  def not_found_error
    # TODO

    head :ok
  end
end
