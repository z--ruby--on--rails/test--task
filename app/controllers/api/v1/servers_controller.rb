# frozen_string_literal: true

class Api::V1::ServersController < Api::V1::ApiController
  def index
    pagy, servers = pagy(Server.all, items: params[:per_page])

    data = ActiveModelSerializers::SerializableResource.new(servers, each_serializer: ServerSerializer)

    render json: { data: data, pagination: pagy_metadata(pagy) }
  end
end
