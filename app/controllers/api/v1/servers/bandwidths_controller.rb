# frozen_string_literal: true

class Api::V1::Servers::BandwidthsController < Api::V1::ApiController
  def index
    pagy, bandwidths = pagy(Bandwidth.where(['server_id = :s', { s: params['server_id'] }]), items: params[:per_page])

    data = ActiveModelSerializers::SerializableResource.new(bandwidths, each_serializer: BandwidthSerializer)

    render json: { data: data, pagination: pagy_metadata(pagy) }
  end
end
