# frozen_string_literal: true

class BandwidthsController < ApplicationController
  def index
    @pagy, @bandwidths = pagy(Bandwidth.where(['server_id = :s', { s: params['server_id'] }]), items: params[:per_page])
  end
end
