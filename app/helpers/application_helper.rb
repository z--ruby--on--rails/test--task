# frozen_string_literal: true

module ApplicationHelper
  include Pagy::Backend
  include Pagy::Frontend
end
