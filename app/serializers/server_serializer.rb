# frozen_string_literal: true

class ServerSerializer < ActiveModel::Serializer
  attributes :id, :name
end
